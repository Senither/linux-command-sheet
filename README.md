Linux Command Sheet
===================

Linux command sheet is a simple web application that was made for a friend that doesn't work much with Linux, but on occasion needs to dabble in it a bit.

He is a [Minecraft](https://minecraft.net/en/) server owner and runs some custom scripts on his machines, among them are [Minecraft Server Scripts](https://bitbucket.org/Senither/minecraft-server-scripts), because of this, some commands that wont work on other machines might show up on the command sheets.

## Issues

Feel free to submit issues if you find any spelling mistakes, or if you want something added to the project.

## Contributing

Contributing to the command sheets is also very welcomed, you can fork the project and add your own command sheets by creating new files in the _sheets_ folder, the file has to be a _.PHP_ file, and it **MUST** return an array in order to be loaded correctly.

## License

The project is licensed under the [MIT](https://opensource.org/licenses/MIT) license.