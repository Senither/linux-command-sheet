<?php 

// Setting up files that we'll need later on
$path   = __DIR__ . DIRECTORY_SEPARATOR  . 'sheets';
$folder = new DirectoryIterator($path);
$ext    = '.php';
$sheets = [];

/**
 * Checks if the given file is a PHP file by comparing
 * it's extension with the $ext variable.
 *
 * @var String $file - The file that should be checked.
 */
$isPHPFile = function ($file) use ($ext) {
    return ($temp = strlen($file) - strlen($ext)) >= 0 && strpos($file, $ext, $temp) === false;
};

// Loops through all of our files using the Directory Iterator.
foreach ($folder as $file) {
    // If the file is a dot entry we'll want to skip it
    if ($file->isDot()) {
        continue;
    }
    
    // Checks if the file is a PHP file, if it isn't we'll skip it
    if ($isPHPFile($fileName = $file->getFilename())) {
        continue;
    }

    // Gets the result from the file and checks if the result is an array, if it isn't we'll skip it
    $array = require_once $path . DIRECTORY_SEPARATOR . $fileName;
    if (!is_array($array)) {
        continue;
    }

    // Adds the array to the $sheets variable, using the file name without the extension as the key
    $sheets[mb_substr($fileName, 0, mb_strlen($fileName) - 4)] = $array;
}

// Returns the sheets data to the index.php file so we can generate our command sheets.
return $sheets;
