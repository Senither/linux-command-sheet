<?php 

return [
    'title' => 'File Operations',
    'items' => [
        [
            'items' => [
                'touch <file>' => 'creates empty file with the provided name.',
                'cat <file1> <file2> <...>' => 'Concat-enate files and output.',
                'file <file>' => 'Gets type of the provided file.',
                'cp <file1> <file2>' => 'Copy file1 to file2.',
                'mv <file1> <file2>' => 'Move file1 to file2.',
                'rm <file1>' => 'Delete file1',
                'head <file>' => 'Show the first 10 lines of the provided file.',
                'tail <file>' => 'Shows the last 10 lines of the provided file.',
                'tail -f <file>' => 'Output last lines of the provided file as it changes.'
            ]
        ]
    ]
];
