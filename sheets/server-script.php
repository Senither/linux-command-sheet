<?php 

return [
    'title' => 'Server Script',
    'items' => [
        [
            'items' => [
                'server --help' => 'Displays the help menu.',
                'server <server> --start' => 'Starts the provided server.',
                'server <server> --stop' => 'Stops the provided server.',
                'server <server> --restart' => 'Restarts the provided server.',
                'server <server> --maintenance' => 'Restarts the server a with maintenance countdown.',
            ]
        ]
    ]
];
