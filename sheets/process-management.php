<?php 

return [
    'title' => 'Process Management',
    'items' => [
        [
            'items' => [
                'ps' => 'Show snapshot of processes.',
                'htop' => 'Show real time processes',
                'kill <pid>' => 'Kill process with the provided PID.',
                'pkill <name>' => 'Kill process with the provided name.',
                'killall <name>' => 'Kill all processes names beginning with name.',
            ]
        ]
    ]
];
