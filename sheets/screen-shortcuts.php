<?php 

return [
    'title' => 'Screen Shortcuts',
    'items' => [
        [
            'items' => [
                'screen -S <name>' => 'Start a screen session.',
                'screen -r <name>' => 'Resume a screen session.',
                'screen -ls' => 'Show your current screen sessions.',
                'screen -Xr <name> kill' => 'Force kills a screen session.',
                'CTRL-A-D' => 'Force kills a screen session.',
                'CTRL-A + :' => 'Activate commands for screen.',
            ]
        ]
    ]
];
