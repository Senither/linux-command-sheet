<?php 

return [
    'title' => 'Vim',
    'items' => [
        [
            'title' => 'Opening files',
            'items' => [
                'vim <name>' => 'Opens file or directory in VIM.'
            ]
        ],
        [
            'title' => 'Resources',
            'link' => [
                'url' => 'http://vim.rtorr.com/',
                'txt' => 'Read more about VIM at rtorr.com'
            ]
        ]
    ]
];
