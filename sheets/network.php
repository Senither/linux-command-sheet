<?php 

return [
    'title' => 'Network Commands',
    'items' => [
        [
            'title' => 'Who is online?',
            'items' => [
                'w' => 'Shows who is logged in and what they\'re doing',
                'who' => 'Shows who is logged on.'
            ]
        ],
        [
            'title' => 'Networking',
            'items' => [
                'iptraf' => 'Lists network activity in real-time.',
                'netstat -tulpn' => 'Shows all applications listening on a port.',
                'fuser <port>/<protocol>' => 'Shows the PID for the process running on the port.',
                'ps aux | grep <pid>' => 'Shows the owner of a porcess PID.'
            ]
        ]
    ]
];
