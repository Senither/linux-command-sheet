<?php 

return [
    'title' => 'Directory Operations',
    'items' => [
        [
            'items' => [
                'pwd' => 'Shows currency directory.',
                'mkdir <name>' => 'Makes directory with the given name.',
                'cd <name>' => 'Changes directory.',
                'cd ..' => 'Go up a directory',
                'ls' => 'List files',
                'll' => 'List files and their permissions',
            ]
        ]
    ]
];
