<?php 

return [
    'title' => 'Bash Shortcuts',
    'items' => [
        [
            'items' => [
                'CTRL-C' => 'Stops current command/program.',
                'CTRL-Z' => 'Changes program state to sleep.',
                'CTRL-A' => 'Go to start of line.',
                'CTRL-E' => 'Go to end of line.',
                'CTRL-U' => 'Cut from start of line.',
                'CTRL-K' => 'Cut to end of line.',
                'CTRL-R' => 'Search history.',
                'fg' => 'Wakes up program state from sleep.',
                '!!' => 'Repeat last comand',
                '!abc' => 'Run last command starting with abc',
                '!abc:p' => 'Print last command starting with abc',
                '!$' => 'Last argument of previous command',
                '!*' => 'All arguments of previous command',
                '^abc-^-123' => 'Run previous command, replacing abc with 123',
            ]
        ]
    ]
];
