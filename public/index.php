<?php $sheets = require_once '../Sheets.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Linux Command Line Cheat Sheet</title>
        <link rel="stylesheet" href="assets/css/styles.css" type="text/css" />
        <script type="text/javascript" src="assets/js/jquery.1.7.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.masonry.min.js"></script>
        <script type="text/javascript" src="assets/js/main.js"></script>
    </head>
    <body class="cheat-detail">

        <div id="navbar">
            <a id="logo" href="#">Linux Command Sheet</a>

            <ul >
                <li>
                    <a href="https://bitbucket.org/Senither/linux-command-sheet">Contribute to the command sheet via BitBucket.</a>
                </li>
            </ul>
        </div>

        <div class="loading">
            Loading...
        </div>

        <div class="content">
            <?php foreach ($sheets as $sheet): ?>
                <div class="board">
                    <h2 class="board-title"><?php echo $sheet['title']; ?></h3>
                    <?php foreach ($sheet['items'] as $items): ?>
                        <div class="board-card">
                            <?php if (isset($items['title'])): ?>
                                <h3 class="board-card-title"><?php echo $items['title']; ?></h3>
                            <?php endif; ?>
                            <ul>
                                <?php if (isset($items['items'])): foreach ($items['items'] as $command => $tip): ?>
                                    <li><?php echo htmlentities($command); ?></li>
                                    <li class="tip"><?php echo htmlentities($tip); ?></li>
                                <?php endforeach; endif; ?>

                                <?php if (isset($items['link'])): ?>
                                    <li><a href="<?php echo $items['link']['url']; ?>"><?php echo $items['link']['txt']; ?></a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </body>
</html>
