/**
 * Linux Command Sheet - CSS Main 
 */
$(function () {
    // Animates the content of the page in using JQuery
    $('.loading').remove();
    $('.content').fadeIn('fast');
    
    // Creates our board items array so we can interact with them later
    var items = $('.board');

    // Lists the boards using Masonry, listing the boards 
    // where there is vertical available space.
    $('.content').masonry({
        itemSelector: '.board',
        isFitWidth: true,
        isAnimated: ! $.browser.msie
    });

    // Creates an array of RGBA colours that we wanna use as 
    // the background for our boards
    var rgba = [
        'rgba(251,34,240,0.25)',
        'rgba(214,17,21,0.25)',
        'rgba(14,251,252,0.25)',
        'rgba(158,134,255,0.25)',
        'rgba(60,255,20,0.25)',
        'rgba(44,158,52,0.25)',
        'rgba(225,211,20,0.25)',
        'rgba(100,117,121,0.25)',
    ];

    // Loops through all of our boards, applying a background colour to them
    $.each(items, function (index, item) {
        // If the index exceeds or is equal to our RGBA array we'll cut it 
        // off so that the patteren repeats itself without too much fuss
        if (index >= rgba.length) {
            index = Math.floor(index / rgba.length) - 1;
        }

        // Applies the background to the board
        $(item).css('background', rgba[index]);
    });
});